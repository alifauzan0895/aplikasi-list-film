import React, { Component } from 'react';
import { Text, View, Button, TextInput, TouchableOpacity } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export default class SignUp extends Component {
    constructor() {
        super()
        this.state = {
            secure: true,
            data: {},
            email: '',
            password: ',',
            currentuser: '',
        }
    }

    componentDidMount() {}

    functionAuth() {
        const { email, password } = this.state;
        auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
                console.log('User account created & signed in!');
            })
            .catch(error => {
                if (error.code === 'auth/email-already-in-use') {
                    console.log('That email address is already in use!');
                }
                if (error.code === 'auth/invalid-email') {
                    console.log('That email address is invalid!');
                }
                console.error(error);
            })
    }


    render() {
        const { email, password } = this.state;
        return (
            <View style={{ flex: 1 }}>
              
                <View style={{ alignSelf: 'center', justifyContent: 'center', marginTop: 180 }}>
                    <Text style={{ fontSize: 30, fontWeight: 'bold', color: 'black' }}>
                        Sign Up Page.
                    </Text>
                </View>
                <View style={{ marginTop: 30, alignSelf: 'center', justifyContent: 'center' }}>
                    <TextInput
                        onChangeText={value => this.setState({ email: value })}
                        style={{ fontWeight: 'bold', width: 200, height: 40, paddingLeft: 50, borderRadius: 40, backgroundColor: 'grey' }}
                        placeholder='Your Email Here'
                        placeholderTextColor='black' />
                    <TextInput
                        onChangeText={value => this.setState({ password: value })}
                        style={{ fontWeight: 'bold', width: 200, height: 40, marginTop: 20, paddingLeft: 40, borderRadius: 40, backgroundColor: 'grey' }}
                        placeholder='Your Password Here'
                        placeholderTextColor='black' />
                </View>
                <View style={{ marginTop: 10, }}>
                    <Button
                        onPress={() => { this.functionAuth() }}
                        style={{ width: 100, height: 30 }}
                        title='Login'
                    />
                </View>
            </View>
        )
    }
 }
