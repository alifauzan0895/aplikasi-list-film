import React, {Component} from 'react';
import {
  StyleSheet,
  Image,
  ImageBackground,
  Text,
  View,
  ScrollView,
} from 'react-native';
import axios from 'axios';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
     list:[]
    };
  }
  componentDidMount() {
 axios.get(`https://dummyjson.com/users`)
      .then(res=> this.setState({list:res.data.users}))

   
      
  }
  render() {
    const {list} = this.state;
    console.log(list);
    return (
      <View style={styles.container}>
        <ImageBackground
          source={{
            uri: 'https://i.pinimg.com/236x/84/f1/5b/84f15ba92862b8345199329c63025f50.jpg',
          }}>
          <ScrollView>
            
              {list &&
               list.map((value, index) => {
                  return (
                    <View style={styles.k} key={index}>
                      <View>
                        <View style={styles.g}>
                          <Image
                            style={{width: 50, height: 50}}
                            source={{uri: value.image}}
                          />
                          <View style={styles.h}>
                            
                              <Text style={styles.q}>email: {value.email}</Text>
                              <Text style={styles.q}> sandi : {value.password}</Text>
                              <Text style={styles.q}> rambut: {value.hair.color}</Text>

                              <Text style={styles.q}> gaya : {value.hair.type}</Text>
                            
                          </View>
                        </View>
                      </View>
                    </View>
                  );
                })}
            
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}
const styles = {
  container: {
    // flexDirection:'row'
  },
  q: {
    fontSize: 20,
    color: 'white',
    borderWidth: 3,
    borderRadius: 10,
    padding: 20,
  },

  h: {
    // borderRadius:30,
    justifyContent: 'space-around',
    margin: 5,
    flexShrink: 1,

    // alignItems:'center',
    // marginRight:5,
    // // borderColor:'red',
    // borderWidth:5,
    // padding:20,
    // flexShrink:1,
    // padding:5,
  },
  g: {
    flexDirection: 'row',
    // backgroundColor:'#FFA500',
    borderRadius: 30,
    borderColor: '#7FFD4',
    borderWidth: 5,
    padding: 30,
    marginVertical: 5,
    // backgroundColor:'#ADD8E6',
    shadowOpacity: 0.3,
    opacity: 0.85,
    justifyContent: 'center',
  },
};
