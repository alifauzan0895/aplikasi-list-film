// import React from 'react';
// import {StyleSheet, Text, View, Button, TouchableOpacity} from 'react-native';

// export default class App extends React.Component {
//   constructor() {
//     super();
//     this.state = {
//       resultText: '',
//       calculationText: '',
//     };
//     this.operations = ['DEL', '+', '-', 'x', '/'];
//   }

//   calculateResult() {
//     const text = this.state.resultText;
//     // do magic
//     this.setState({
//       calculationText: eval(text),
//     });
//   }
//   validate() {
//     const text = this.state.resultText;
//     switch (text.slice(-1)) {
//       case '+':
//       case '-':
//       case 'x':
//       case '/':
//         return false;
//     }
//     return true;
//   }

//   buttonPressed(text) {
//     if (text == '=') {
//       return this.validate() && this.calculateResult();
//     }
//     this.setState({
//       resultText: this.state.resultText + text,
//     });
//   }

//   operate(operation) {
//     switch (operation) {
//       case 'DEL':
//         let text = this.state.resultText.split('');
//         text.pop();
//         this.setState({
//           resultText: text.join(''),
//         });
//         break;
//       case '+':
//       case '-':
//       case 'x':
//       case '/':
//         const lastChar = this.state.resultText.split('').pop();

//         if (this.operations.indexOf(lastChar) > 0) return;

//         if (this.state.resultText == '') {
//           return;
//         }
//         this.setState({
//           resultText: this.state.resultText + operation,
//         });
//     }
//   }

//   render() {
//     let rows = [];
//     let nums = [
//       [1, 2, 3],
//       [4, 5, 6],
//       [7, 8, 9],
//       ['.', 0, '='],
//     ];
//     for (let i = 0; i < 4; i++) {
//       let row = [];
//       for (let j = 0; j < 3; j++) {
//         row.push(
//           <TouchableOpacity key={nums[i][j]} style={styles.btn}>
//             <Text
//               onPress={() => this.buttonPressed(nums[i][j])}
//               style={styles.btntext}>
//               {nums[i][j]}
//             </Text>
//           </TouchableOpacity>,
//         );
//       }
//       rows.push(
//         <View key={i} style={styles.row}>
//           {row}
//         </View>,
//       );
//     }

//     let ops = [];
//     for (let i = 0; i < 5; i++) {
//       ops.push(
//         <TouchableOpacity
//           key={this.operations[i]}
//           style={styles.btn}
//           onPress={() => this.operate(this.operations[i])}>
//           <Text style={[styles.btntext, styles.white]}>
//             {this.operations[i]}
//           </Text>
//         </TouchableOpacity>,
//       );
//     }

//     return (
//       <View style={styles.container}>
//         <View style={styles.result}>
//           <Text style={styles.resultText}>{this.state.resultText}</Text>
//         </View>
//         <View style={styles.calculation}>
//           <Text style={styles.calculationText}>
//             {this.state.calculationText}
//           </Text>
//         </View>
//         <View style={styles.buttons}>
//           <View style={styles.numbers}>{rows}</View>
//           <View style={styles.operations}>{ops}</View>
//         </View>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
//   row: {
//     flexDirection: 'row',
//     flex: 1,
//     justifyContent: 'space-around',
//     alignItems: 'center',
//   },
//   btntext: {
//     fontSize: 30,
//   },
//   white: {
//     color: 'white',
//   },
//   btn: {
//     flex: 1,
//     alignItems: 'center',
//     alignSelf: 'stretch',
//     justifyContent: 'center',
//   },
//   calculationText: {
//     fontSize: 24,
//     color: 'white',
//   },
//   resultText: {
//     fontSize: 30,
//     color: 'white',
//   },
//   result: {
//     flex: 2,
//     backgroundColor: 'red',
//     justifyContent: 'center',
//     alignItems: 'flex-end',
//   },
//   calculation: {
//     flex: 1,
//     backgroundColor: '#4ba0f4',
//     justifyContent: 'center',
//     alignItems: 'flex-end',
//   },
//   buttons: {
//     flex: 7,
//     flexDirection: 'row',
//   },
//   numbers: {
//     flex: 3,
//     backgroundColor: '#82868c',
//   },
//   operations: {
//     flex: 1,
//     justifyContent: 'space-around',
//     alignItems: 'stretch',
//     backgroundColor: 'green',
//   },
// });

// import React from 'react';
// import {
//   StyleSheet,
//   Image,
//   Text,
//   View,
//   TextInput,
//   TouchableOpacity,
// } from 'react-native';
// import Cbutton from '../../../Component/atom';

// export default class index extends React.Component {
//   state = {
//     email: '',
//     password: '',
//   };
//   render() {
//     return (
//       <View style={styles.container}>
//         <Image
//           source={{
//             uri: 'https://png.pngtree.com/background/20210709/original/pngtree-vector-graffiti-book-creative-educational-background-blackboard-picture-image_548339.jpg',
//           }}
//           style={styles.Container}
//         />
//         <Text style={styles.logo}>log in</Text>

//         <View style={styles.inputView}>
//           <TextInput
//             style={styles.inputText}
//             placeholder="Email..."
//             placeholderTextColor="#6BB3DA"
//             onChangeText={text => this.setState({email: text})}
//           />
//         </View>
//         <View style={styles.inputView2}>
//           <TextInput
//             secureTextEntry
//             style={styles.inputText}
//             placeholder="Password..."
//             secureTextEntry={true}
//             placeholderTextColor="#6BB3DA"
//             onChangeText={text => this.setState({password: text})}
//           />
//         </View>
// <Cbutton style={{backgroundColor:'black'}} title='bismillah'/>
//         <TouchableOpacity>
//           <Text style={styles.forgot}>{this.state.email}</Text>
//         </TouchableOpacity>


// {this.state.email && this.state.password &&  <TouchableOpacity
//           onPress={() => {
//             this.props.navigation.navigate('listfilm');
//           }}
//           style={styles.loginBtn}>
//           <Text style={styles.loginText}>LOGIN</Text>
//         </TouchableOpacity>}




       
//         <TouchableOpacity>
//           <Text style={styles.loginText}>Signup</Text>
//         </TouchableOpacity>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#BDC21B',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   logo: {
//     fontWeight: 'bold',
//     fontSize: 50,
//     color: '#57FEFF',
//     marginBottom: 40,
//   },
//   inputView: {
//     width: '80%',
//     backgroundColor: '#465881',
//     borderRadius: 20,
//     height: 50,
//     marginBottom: 20,
//     justifyContent: 'center',
//     padding: 10,
//   },
//   inputView2: {
//     width: '80%',
//     backgroundColor: '#465881',
//     borderRadius: 20,
//     height: 50,
//     marginBottom: 20,
//     justifyContent: 'center',
//     padding: 10,
//   },
//   inputText: {
//     height: 60,
//     color: 'white',
//   },
//   forgot: {
//     color: 'white',
//     fontSize: 11,
//   },
//   loginBtn: {
//     width: '80%',
//     backgroundColor: '#fb5b5a',
//     borderRadius: 25,
//     height: 50,
//     alignItems: 'center',
//     justifyContent: 'center',
//     marginTop: 40,
//     marginBottom: 10,
//   },
//   loginText: {
//     color: '#57FEFF',
//   },
//   // uwie:{
//   //   flexDirection:'row-reverse'

//   //
// });
import React, { Component } from 'react'
import {  Button, StyleSheet, Text, TextInput, View } from 'react-native'
import  AsyncStorage  from '@react-native-async-storage/async-storage'

export default class index extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            textName: '',
            textemail: ''
        };

        AsyncStorage.getItem('user', (error, result) => {
            if (result) {
                let resultParsed = JSON.parse(result)
                this.setState({
                    name: resultParsed.name,
                    email: resultParsed.email
                });
            }
        });
    }

    // Menyimpan ke local data, untuk test kamu bisa close aplikasi atau mematikan telepon mu dan lihat kembali data masih ada
    saveToLocal() {
        let name = this.state.textName;
        let email = this.state.textemail;
        let data = {
            name: name,
            email: email
        }
// Menyimpan data ke storage
        AsyncStorage.setItem('user', JSON.stringify(data));

        this.setState({
            name: name,
            email: email
        });

        alert('Data tersimpan');
    }

    //  Mengahpus Data Local Storage, untuk meyakinkan dan tes silahkan close aplikasi dan buka lagi maka data akan hilang
    async removeItemValue() {
      try {
        // Mengahpus data kdari local storage
          await AsyncStorage.removeItem('user');
         alert('Berhasil Menghapus Data')
      }
      catch(exception) {
        console.log(exception)
         alert('Gagal Menghapus Data')
      }
  }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.textWho}>
                    Siapa Kamu ?.
                </Text>
                <Text style={styles.viewResult}>
                    Nama: {this.state.name}{'\n'}
                  Email: {this.state.email}
                </Text>
                <TextInput style={styles.textInput}
                    onChangeText={(textName) => this.setState({textName})}
                    placeholder='Nama'
                />
                <TextInput style={styles.textInput}
                    onChangeText={(textemail) => this.setState({textemail})}
                    placeholder='Email'
                    keyboardType='email-address'
                />
                <Button
                    title='Simpan'
                    onPress={this.saveToLocal.bind(this)}
                    color='green'
                />
            <View style={styles.viewRemove}> 
              <Button
                    title='Hapus Storage'
                    onPress={this.removeItemValue}
                    color='red'
                />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: 16,
    paddingTop: 32
  },
  textWho:{
    fontWeight:'bold',
    fontSize:16
  },
  viewResult:{
    flexDirection:'row'
  },

  textInput: {
    height: 40,
    width:300,
    backgroundColor: 'white',
    marginTop: 8,
    marginBottom: 8,
    borderWidth: 1,
    borderColor: '#ddd',
    padding: 8,
    borderRadius:10
  },
  viewRemove:{
    marginTop:20
  }
});





