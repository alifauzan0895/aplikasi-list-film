import React, { Component } from 'react';
import { Text, StyleSheet, View, TouchableOpacity,StatusBar} from 'react-native';

export default class index extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-start',
          alignItems: 'flex-start',
          
   

        }}>
             <View style={{justifyContent:'center',alignItems:'center'}}> 
             <Text>JANGAN DI SENTUH</Text>
             </View>
        <View style={{ flexDirection: 'row' }}>
          <StatusBar 
          barStyle='light-content'
          backgroundColor="#fff"
          hidden={true}
          />

          <View style={styles.login}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('login');
              }}>
              <Text> login</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.Notif}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Notif');
              }}>
              <Text> notification</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.calculator}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('calculator');
              }}>
              <Text> calculator</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.film}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('listfilm');
              }}>
              <Text> film</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View><TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('inbox');
              }}>
              <Text> inbox</Text>
            </TouchableOpacity></View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  login: {
    width: '25%',
    backgroundColor: '#4658D1',
  
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    borderRadius:7,
    padding: 10,
  },
  calculator: {
    width: '25%',
    backgroundColor: '#45f881',
   
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 10,
    borderRadius:7,
  },
  film: {
    width: '25%',
    backgroundColor: '#66b881',
    borderRadius:7,
   
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 10,
  },
  Notif: {
    width: '25%',
    backgroundColor: '#6681',
    borderRadius:7,
   
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    padding: 10,
  }
  
});

