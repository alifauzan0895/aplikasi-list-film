import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

export default class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bilangan: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 23, 24]
        }

    }

    render() {
        const { bilangan } = this.state
        return (
            <View >
                <View style={{ flexDirection: 'row' }}>
                    <View>
                        {bilangan.map((v, index) => {
                            return (
                                <View >
                                    {v % 2 != 0 && <Text>
                                        ganjil: {v}
                                    </Text>}

                                </View>
                            )
                        })}

                    </View>

                    <View >
                        {bilangan.map((v, index) => {
                            return (
                                <View >
                                    {v % 2 == 0 && <Text>
                                        genap: {v}
                                    </Text>}


                                </View>
                            )
                        })}
                    </View>
                    <View>
                        {bilangan.map((v, index) => {
                            return (
                                <View  >
                                    {v > 1 && v % 2 != 0 && v % 3 != 0 && <Text>
                                        prima: {v}
                                    </Text>}

                                </View>
                            )
                        })}
                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({});
