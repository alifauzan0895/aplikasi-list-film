import { Text, StyleSheet, View } from 'react-native';
import React, { Component } from 'react';

export default class index extends Component {
    constructor() {
        super();
        this.state = {
          data: [],
          name: '',
          address: '',
          // id: '',
          onSelect: false,
          onEdit: false,
          checked: true,
        };
      }
    
      componentDidMount() {}
    
      _addData = () => {
        const {name, address} = this.state;
        const id = unique_id();
        const data = {name, address, id};
        this.props.add(data);
        this.setState({
          name: '',
          address: '',
          id: '',
        });
      };
    
      _onEdit = value => {
        this.setState({
          onEdit: !this.state.onEdit,
          id: value.id,
          name: value.name,
          address: value.address,
        });
      };
    
      _delete = id => {
        this.props.delete(id);
      };
    
      _submitUpdate = () => {
        const {id, name, address} = this.state;
        const {updateData, students} = this.props;
        const data = {id, name, address};
        const updateStudents = students.map(value => {
          if (value.id === data.id) {
            (value.id = data.id),
              (value.name = data.name),
              (value.address = data.address);
            return value;
          } else {
            return value;
          }
        });
        console.log(updateStudents)
        updateData(updateStudents);
        this.setState({
          onEdit: false,
          name: '',
          address: '',
        });
      };
    
      _cancel = () => {
        this.setState({
          name: '',
          id: '',
          address: '',
          onEdit: false,
        });
      };
  render() {
    return (
      <View>
        <Text></Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({});
