import * as React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../screen/login'
import Robot from '../screen/robot/page'
import Home from '../screen/home'
import Listfilm from '../screen/listfilm'
import Nama from '../screen/nama' 
import Film from '../screen/listfilm/film' 
import Splash from '../splash/'
import Table from '../screen/CRUD'
import Latihan from '../screen/tugas'
import Bilanganprima from '../screen/tugas/bilangan prima'
import Notif from '../screen/firebase/notification/index'
import Riding from '../screen/redux/'
import test from '../screen/firebase/tes'

const Stack = createNativeStackNavigator();
const hide = {headerShown: false}
function index() {
 
return (
 
<NavigationContainer>
<Stack.Navigator>
  {/* <Stack.Screen options={{headerShown:false}} name ="bilangan"component={Bilanganprima}/>
<Stack.Screen name="latihan" component={Latihan} />  */}
<Stack.Screen options={{headerShown:false}} name="splash" component={Splash} /> 
<Stack.Screen name="Table" component={Table} /> 
<Stack.Screen name="Notif" component={Notif} /> 
<Stack.Screen name="redux" component={Riding} /> 
<Stack.Screen name="inbox" component={test} /> 



<Stack.Screen name="home" component={Home} options={hide} />

<Stack.Screen name="nama" component={Nama} />
<Stack.Screen name="calculator" component={Robot} />
<Stack.Screen name="login" component={Login} />
<Stack.Screen name="film" component={Film} />
<Stack.Screen options={{headerShown:false}} name="listfilm" component={Listfilm} /> 



</Stack.Navigator>
</NavigationContainer>
);
}
export default index;