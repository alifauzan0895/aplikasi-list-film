import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';
import {colors, Placeholder} from '../../assets';

export default class Inbox extends Component {
  render() {
    const {body, title, data, isRead, image} = this.props;
    return (
      <TouchableOpacity
        {...this.props}
        activeOpacity={0.7}
        style={[
          styles.main,
          isRead ? styles.readContainer : styles.unreadContainer,
        ]}>
        <View>
          <Image style={styles.image} source={image ? image : Placeholder} />
        </View>
        <View>
          <Text style={[isRead ? styles.readText : styles.unreadText]}>
            {title}
          </Text>
          <Text>{body}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 15,
    flexDirection: 'row',
  },
  unreadContainer: {
    backgroundColor: colors.primary,
    borderRadius: 20,
  },
  unreadText: {
    fontWeight: 'bold',
  },
  image: {
    width: 75,
    height: 75,
    borderRadius: 10,
    marginRight: 10
  },
});
