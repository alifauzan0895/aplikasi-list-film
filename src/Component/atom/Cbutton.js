import React, {Component} from 'react';
import {Text, StyleSheet, View, TouchableOpacity} from 'react-native';

export default class Cbutton extends Component {
  render() {
    return (
      <View>
        <TouchableOpacity style={this.props.style}{...this.props}>
          
        
        <Text>{this.props.title}</Text>
      </TouchableOpacity>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 5,

    backgroundColor: '#ffae00',

    maxWidth: 100,

    borderRadius: 20,

    justifyContent: 'center',

    alignItems: 'center',
  },
});
