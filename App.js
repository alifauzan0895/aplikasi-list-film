import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/redux/store';
import Navigation from './src/Navigation/index'
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';

  // foreground
messaging().onMessage(async remoteMessage => {
  // Store.dispatch({type:'Inbox',payload:[
  //   {
  //     title: remoteMessage.notification.title,
  //     body: remoteMessage.notification.body,
  //     data: remoteMessage.data,
  //     image: remoteMessage.notification.android.imageUrl,
  //     isRead: 0
  //   }
  // ]})
    // setInboxData(remoteMessage)
  });

  // background
  messaging().setBackgroundMessageHandler(async remoteMessage => { 
    setInboxRedux(remoteMessage)

    // Store.dispatch({type:'Inbox',payload:[
    //   {
    //     title: remoteMessage.notification.title,
    //     body: remoteMessage.notification.body,
    //     data: remoteMessage.data,
    //     image: remoteMessage.notification.android.imageUrl,
    //     isRead: 0
    //   }
    // ]})
    // setInboxData(remoteMessage)
    const setInboxRedux = async remoteMessage => {
      Store.dispatch({
        type: 'Inbox',
        payload: {
          title: remoteMessage.notification.title,
          body: remoteMessage.notification.body,
          image: remoteMessage.notification.android.imageUrl,
          time: time,
          data: remoteMessage.data,
          isRead: 0,
          id: Math.round(Math.random() * 1000000),
        },
      });
    };
    
  });

  
  const setInboxData = async (remoteMessage) => { console.log(remoteMessage)
    try {
      let getInboxData = await AsyncStorageLib.getItem('newInboxData');
      getInboxData = JSON.parse(getInboxData);

      if (!getInboxData) {
        AsyncStorageLib.setItem('newInboxData',
          JSON.stringify(
            [
              {
                title: remoteMessage.notification.title,
                body: remoteMessage.notification.body,
                data: remoteMessage.data,
                image: remoteMessage.notification.android.imageUrl,
                isRead: 0
              }
            ]
          )
        )
      } else {
        const arrayData = [
          ...[
            {
              title: remoteMessage.notification.title,
              body: remoteMessage.notification.body,
              data: remoteMessage.data,
              image: remoteMessage.notification.android.imageUrl,
              isRead: 0
            }
          ],
          ...getInboxData
        ].slice(0, 20)
        AsyncStorageLib.setItem('newInboxData',
          JSON.stringify(arrayData)
        )
      }
    } catch (error) {
      console.log(error);
    }
  }
  



const App = () => {
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};

export default App;



// import { Value } from 'animated';
// import React, { Component } from 'react'
// import { Text, StyleSheet, View, ScrollView, TextInput } from 'react-native'
// import Cbutton from './src/Component/atom/Cbutton';

// export default class App extends Component { */}
//   constructor(props) {
//     super();
//     this.state = {
//       data: [
//         { id: 1, nama: 'mahrus', alamat: 'alambarzah' },
//         { id: 2, nama: 'hardi', alamat: 'alam mahsyar' },


//       ],
//       nama: '',
//       alamat: '',
//       elemenbaru:{}
//     }
//   }

//   _tambah() {
//     const { nama, almat, data } = this.state
//     this.setState({elemenbaru:{nama:this.state.nama},elemenbaru:[...data,{nama,alamat}]})
    
//   };
//   _edit() { };
//   // _hapus = (id, nama, alamat) => {
//   //   const { nama, alamat, data }=this.state
//   //   const datamumet = data.filter(value => { return value.id != id })
//   //   this.Setstate({
//   //     data: datamumet
//   //   });


//   // }

//   render() {
//     const { data, nama, alamat } = this.state;


//     return (
//       <View>
//         <ScrollView>


//           <View>
//             {data.map((value, i) => {
//               return (

//                 <View key={i}>
//                   <View style={{ flexDirection: 'row' }}>
//                     <Text  > {value.nama}</Text>
//                     <Text> {value.alamat}</Text>
//                   </View>
//                 </View>
//                ) } )}
//           </View>

//           <View>
//           <TextInput
//             style={styles.inputText}
//             placeholder="nama"
//             placeholderTextColor="#6BB3DA"
      
//           />
//             <TextInput 
//              style={styles.inputText}
//              placeholder="alamat"
//              placeholderTextColor="#6BB3DA"
            
//             />
                   
//           </View>
// <View style={{borderRadius:60,backgroundColor:'red',width:60,height:30, }}>

//   <Cbutton title='pencet'  />
// </View>
//         </ScrollView>
//  </View>

//     )
//   }
// }

// const styles = StyleSheet.create({})
// import React, {Component} from 'react';
// import {
//   StyleSheet,
//   Image,
//   ImageBackground,
//   Text,
//   View,
//   ScrollView,
// } from 'react-native';
// import axios from 'axios';

// export default class App extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//      list:[]
//      ,username:''
//      ,password:''
//     };
//   }
//   componentDidMount() {
//  axios.get(`https://dummyjson.com/users`)
//       .then(res=> this.setState({list:res.data.users}))
    

    


      
//     // } 
//     // _submit(){
     
//     //     const {list}=this.state
//     //     const saring=list.filter(res=>{
//     //       return(
//     //         res.username==this.state.username &&
//     //         res.password==this.state.password
//     //       )
//     //     })
     
//     //     console.log(saring);

      
//   }
//   render() {
//   const res=this.state
//   console.log(res)
//     return (
//       <View style={styles.container}>
//         <ImageBackground
//           source={{
//             uri: 'https://i.pinimg.com/236x/84/f1/5b/84f15ba92862b8345199329c63025f50.jpg',
//           }}>
//           {/* <ScrollView>
            
//               {list &&
//                list.map((value, index) => {
//                   return (
//                     <View style={styles.k} key={index}>
//                       <View>
//                         <View style={styles.g}>
//                           <Image
//                             style={{width: 50, height: 50}}
//                             source={{uri: value.image}}
//                           />
//                           <View style={styles.h}>
                            
//                               <Text style={styles.q}>email: {value.email}</Text>
//                               <Text style={styles.q}> sandi : {value.password}</Text>
//                               <Text style={styles.q}> rambut: {value.hair.color}</Text>

//                               <Text style={styles.q}> gaya : {value.hair.type}</Text>
                            
//                           </View>
//                         </View>
//                       </View>
//                     </View>
//                   );
//                 })}
            
//           </ScrollView> */}
//         </ImageBackground>
//       </View>
//     );
//   }
// }
// const styles = {
//   container: {
//     // flexDirection:'row'
//   },
//   q: {
//     fontSize: 20,
//     color: 'white',
//     borderWidth: 3,
//     borderRadius: 10,
//     padding: 20,
//   },

//   h: {
//     // borderRadius:30,
//     justifyContent: 'space-around',
//     margin: 5,
//     flexShrink: 1,

//     // alignItems:'center',
//     // marginRight:5,
//     // // borderColor:'red',
//     // borderWidth:5,
//     // padding:20,
//     // flexShrink:1,
//     // padding:5,
//   },
//   g: {
//     flexDirection: 'row',
//     // backgroundColor:'#FFA500',
//     borderRadius: 30,
//     borderColor: '#7FFD4',
//     borderWidth: 5,
//     padding: 30,
//     marginVertical: 5,
//     // backgroundColor:'#ADD8E6',
//     shadowOpacity: 0.3,
//     opacity: 0.85,
//     justifyContent: 'center',
//   },
// };

// import { Text, Alert, StyleSheet, View } from 'react-native';
// import React, { Component } from 'react';
// import messaging from '@react-native-firebase/messaging';
// import AsyncStorage from '@react-native-async-storage/async-storage'


// export default class App extends Component {

//   componentDidMount() {
//     messaging().onMessage(async remoteMessage => {
//       Alert.alert(
//         'Anda menerima pesan baru : ',
//         JSON.stringify(remoteMessage.notification.body.title),
//       );
//       const title = remoteMessage.notification.title;
//       const body = remoteMessage.notification.text;
//       AsyncStorage.getItem('inbox')
//         .then(resultInbox => {
//           const inbox = JSON.parse(resultInbox);
//           AsyncStorage.setItem(
//             'inbox',
//             JSON.stringify([{title: title, text: body}, ...inbox]),
//           );
//         })
//         .catch(err => {
//           console.log(err);
//         });
//     });
//   }

//   render() {
//     return (
//       <View>
//         <Text>ANDAK RESEK KALAU LAGI LAPER</Text>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({});


